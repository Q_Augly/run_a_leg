const config = require('config.js');
//首页接口
const indexPage = '/index.php?g=api&m=user&a=index',
   //排序与切换与地址
   rank = '/index.php?g=api&m=user&a=rank',
   //图片前缀
   imgUrl = 'http://dididida.net/data/upload/',
   //店铺详情接口
   shopdetail = '/index.php?g=api&m=user&a=details';
App({
   /**
    * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
    */
   onLaunch() {
      
   },
   ajax(url, params, successData, errorData, completeData) {
      var https = "http://dididida.net"
      wx.request({
         url: https + url,
         method: "POST",
         header: {
            'content-type': 'application/x-www-form-urlencoded',
         },
         data: params,
         success: (res) => {
            successData(res)
         },
         error(res) {
            errorData(res)
         }
      })
   },
   /**
    * 当小程序启动，或从后台进入前台显示，会触发 onShow
    */
   onShow: function (options) {

   },

   /**
    * 当小程序从前台进入后台，会触发 onHide
    */
   onHide: function () {

   },
   convertHtmlToText: function convertHtmlToText(inputText) {
      var returnText = "" + inputText;
      returnText = returnText.replace(/<\/div>/ig, '\r\n');
      returnText = returnText.replace(/<\/li>/ig, '\r\n');
      returnText = returnText.replace(/<li>/ig, ' * ');
      returnText = returnText.replace(/<\/ul>/ig, '\r\n');
      //-- remove BR tags and replace them with line break
      returnText = returnText.replace(/<br\s*[\/]?>/gi, "\r\n");

      //-- remove P and A tags but preserve what's inside of them
      returnText = returnText.replace(/<p.*?>/gi, "\r\n");
      returnText = returnText.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 ($1)");

      //-- remove all inside SCRIPT and STYLE tags
      returnText = returnText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
      returnText = returnText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
      //-- remove all else
      returnText = returnText.replace(/<(?:.|\s)*?>/g, "");

      //-- get rid of more than 2 multiple line breaks:
      returnText = returnText.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "\r\n\r\n");

      //-- get rid of more than 2 spaces:
      returnText = returnText.replace(/ +(?= )/g, '');

      //-- get rid of html-encoded characters:
      returnText = returnText.replace(/ /gi, " ");
      returnText = returnText.replace(/&/gi, "&");
      returnText = returnText.replace(/"/gi, '"');
      returnText = returnText.replace(/</gi, '<');
      returnText = returnText.replace(/>/gi, '>');

      return returnText;
   },
   /**
    * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
    */
   onError: function (msg) {

   },
   globalData: {
      bjMoudel: true,// 是否弹框
      userInfo:null,
      imgUrl: imgUrl,
      indexPage: indexPage,
      rank: rank,
      shopdetail: shopdetail,

      allData: [],//首页所有数据
      coordinate:null,//经纬度
      areaid:null,// 所在区域id
      areaidTitle:null,//所在区域名字
      allOrder:null//全部配送订单中的详情allsendorder
   }
})
