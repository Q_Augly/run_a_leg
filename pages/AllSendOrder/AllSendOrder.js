// pages/AllSendOrder/AllSendOrder.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
   //   所有数据
     allData:[],
   //   选项卡类
     activeList:0
  },
//   点击获取列表
   activeList(e){
      wx.showLoading({
         title: '加载中',
      })
      var index = e.currentTarget.dataset.index;
      console.log(index)
      this.setData({
         activeList: index
      })
      app.ajax("/index.php?g=api&m=user&a=gopeoplegoods",{
         states: index,
         userid: app.globalData.allData.userid,
         areaid: app.globalData.areaid
      },(res)=>{
         wx.hideLoading()
         this.setData({
            allData: res.data
         })
      },(res)=>{})
   },
  WaitTake:function(e){
     var id = e.currentTarget.dataset.id;
     var states = e.currentTarget.dataset.states;
     var shopid = e.currentTarget.dataset.shopid;
     
     app.globalData.allOrder = this.data.allData.goods
     if (shopid != 0 && states == 3){
        console.log("配送单-待配送")
        wx.navigateTo({
           url: '/pages/Taking/Taking?id=' + id,
        })
     } else if (shopid == 0 && states == 3){
        console.log("取送件-待配送")
        wx.navigateTo({
           url: '/pages/WaitSend/WaitSend?id=' + id,
        })
     } else if (shopid != 0 && states == 4){
        console.log("配送单-配送中")
        wx.navigateTo({
           url: '/pages/WaitTake/WaitTake?id=' + id,
        })
     } else if (shopid != 0 && states == 5){
        console.log("配送单-已完成")
        wx.navigateTo({
           url: '/pages/TakeFinish/TakeFinish?id=' + id,
        })
     } else if (shopid == 0 && states == 5){
        console.log("取送件-已完成")
        wx.navigateTo({
           url: '/pages/SendFinish/SendFinish?id=' + id,
        })
     }
   //   if (states == 3){
   //      wx.navigateTo({
   //         url: '/pages/WaitSend/WaitSend?id=' + id,
   //      })
   //   } else if (states == 4){
   //      wx.navigateTo({
   //         url: '/pages/WaitSend/WaitSend?id=' + id,
   //      })
   //   }
   //   wx.navigateTo({
   //      url: '/pages/WaitTake/WaitTake?id=' + id,
   //   })
  },
//   WaitTake:function(e){
//     wx.navigateTo({
//       url: '/pages/Taking/Taking',
//     })
//   },
  WaitSend:function(e){
    wx.navigateTo({
      url: '/pages/WaitSend/WaitSend',
    })
  },
  SendFinish:function(e){
    wx.navigateTo({
      url: '/pages/SendFinish/SendFinish',
    })
  },
  TakeFinish:function(e){
    wx.navigateTo({
      url: '/pages/TakeFinish/TakeFinish',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      // 获取全部信息
     app.ajax("/index.php?g=api&m=user&a=gopeoplegoods", {
           states: 0,
           userid: app.globalData.allData.userid,
           areaid: app.globalData.areaid
        }, (res) => {
           console.log(res)
           this.setData({
              allData:res.data
           })
         }, (res) => { })

     
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})