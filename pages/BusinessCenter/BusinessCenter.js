// pages/BusinessCenter/BusinessCenter.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },
  business_info:function(e){
    wx.navigateTo({
      url: '/pages/BusinessInformation/BusinessInformation',
    })
  },
  business_class:function(e){
    wx.navigateTo({
      url: '/pages/BusinessClass/BusinessClass',
    })
  },
  BusinessReduce:function(e){
    wx.navigateTo({
      url: '/pages/BusinessReduce/BusinessReduce',
    })
  },
  BusinessManage:function(e){
    wx.navigateTo({
      url: '/pages/BusinessManage/BusinessManage',
    })
  },
  BusinessWallet:function(e){
    wx.navigateTo({
      url: '/pages/BusinessWallet/BusinessWallet/',
    })
  },
  BusinessMoney:function(e){
    wx.navigateTo({
      url: '/pages/BusinessMoney/BusinessMoney',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})