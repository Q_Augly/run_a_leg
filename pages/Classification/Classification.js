// pages/Classification/Classification.js
var app = getApp()
Page({

   /**
    * 页面的初始数据
    */
   data: {
      imgUrl: app.globalData.imgUrl,
      winHeight: "",//窗口高度
      currentTab: 0, //预设当前项的值
      scrollLeft: 0, //tab标题的滚动条位置
      allData:[],
      // 授权弹框
      bjMoudel: true,
   },
   SearchPage: function (e) {
      wx: wx.navigateTo({
         url: '/pages/SearchPage/SearchPage',
         success: function (res) { },
         fail: function (res) { },
         complete: function (res) { },
      })
   },
   // 滚动切换标签样式
   switchTab: function (e) {
      this.setData({
         currentTab: e.detail.current
      });
      this.checkCor();
      
   },
   // 点击标题切换当前页时改变样式
   swichNav: function (e) {
      var cur = e.target.dataset.current;
      var id = e.target.dataset.id;
      if (this.data.currentTaB == cur) { return false; }
      else {
         this.setData({
            currentTab: cur
         })
      }
      wx.showLoading({
         title: '加载中',
         mask:true
      })
      app.ajax("/index.php?g=api&m=user&a=checkclassify", {
         areaid: app.globalData.areaid,
         indexmenuid:id
      }, (res) => {
         console.log(res)
         wx.hideLoading()
         this.setData({
            "allData.shop":res.data.shop
         })
      }, (res) => { })
   },
   //判断当前滚动超过一屏时，设置tab标题滚动条。
   checkCor: function () {
      if (this.data.currentTab > 4) {
         this.setData({
            scrollLeft: 300
         })
      } else {
         this.setData({
            scrollLeft: 0
         })
      }
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {
      if(options.index){
         this.setData({
            currentTab: options.index
         })
      }
      this.setData({
         bjMoudel:app.globalData.bjMoudel
      })
      var that = this;
      //  高度自适应
      wx.getSystemInfo({
         success: function (res) {
            var clientHeight = res.windowHeight,
               clientWidth = res.windowWidth,
               rpxR = 750 / clientWidth;
            var calc = clientHeight * rpxR - 180;
            console.log(calc)
            that.setData({
               winHeight: calc
            });
         }
      });
      
   },
   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {
      // 分类信息默认页面接口
      app.ajax("/index.php?g=api&m=user&a=classify", {
         areaid: app.globalData.areaid
      }, (res) => {
         console.log(res)
         this.setData({
            allData: res.data
         })
      }, (res) => { })
   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})