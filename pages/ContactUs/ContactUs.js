// pages/ContactUs/ContactUs.js
var app = getApp()
Page({

   /**
    * 页面的初始数据
    */
   data: {
      allData: []
   },
   lyb(e) {
      wx.showLoading({
         title: '上传中',
      })

      var val = e.detail.value;
      app.ajax("/index.php?g=api&m=user&a=leavewords", {
         userid: app.globalData.allData.userid,
         leavewords: val,
         nickname: app.globalData.userInfo.nickName
      }, (res) => {
         wx.showToast({
            title: '反馈成功',
            icon: 'success',
         })
         setTimeout(function () {
            if (res.data.code == 1) {
               wx.hideLoading()
               wx.navigateBack({
                  delta: 1
               })
            }
         }, 2000)

      }, (res) => { })
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {
      app.ajax("/index.php?g=api&m=user&a=connectus", {}, (res) => {
         console.log(res)
         this.setData({
            allData: res.data
         })
      }, (res) => { })
   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {

   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})