// pages/PersonCenter/PersonCenter.js
var app = getApp()
Page({

   /**
    * 页面的初始数据
    */
   data: {
      // 授权弹框
      bjMoudel: true,
      //   所有信息状态
      allData: []
   },
   // 我的订单
   myOrder(e){
      var index = e.currentTarget.dataset.index;
      wx.navigateTo({
         url: '../AllOrder/AllOrder?index='+index,
      })
   },
   //   我的钱包
   myMoney() {
      var types = this.data.allData.user.type
      if (types == 0) {
         wx.navigateTo({
            url: '../PersonWallet/PersonWallet',
         })
      } else if (types == 1) {
         wx.navigateTo({
            url: '../RunnerWallet/RunnerWallet',
         })
      }
   },
   business: function (e) {
      wx.navigateTo({
         url: '/pages/business/business',
      })
   },
   MineAddress: function (e) {
      wx.navigateTo({
         url: '/pages/MineAddress/MineAddress',
      })
   },
   contact_us: function (e) {
      wx.navigateTo({
         url: '/pages/ContactUs/ContactUs',
      })
   },
   AllSendOrder: function (e) {
      wx.navigateTo({
         url: '/pages/AllSendOrder/AllSendOrder',
      })
   },
   AllOrder: function (e) {
      wx.navigateTo({
         url: '/pages/AllOrder/AllOrder',
      })
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {
      this.setData({
         bjMoudel: app.globalData.bjMoudel
      })

      //   用户中心首页接口
      app.ajax("/index.php?g=api&m=user&a=users", {
         userid: app.globalData.allData.userid,
         areaid: app.globalData.allData.areaid,
      }, (res) => {
         console.log(res)
         this.setData({
            allData: res.data
         })
      }, (res) => { })

   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {

   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})