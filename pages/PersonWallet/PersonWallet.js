// pages/PersonWallet/PersonWallet.js
var app = getApp();
Page({

   /**
    * 页面的初始数据
    */
   data: {
      allData: [],
      //   提现弹框
      money_mask: false,
      // 输入提现金额
      val: null
   },
   // 提交提现金额
   sub() {
      var val = this.data.val;
      var token = this.data.allData.token
      // console.log(val)
      // console.log(token)
      if (val == null || val == "") {
         console.log("空")
         wx.showLoading({
            title: '请输入金额',
            mask: true,
            duration: 2000
         })
      } else {
         app.ajax("/index.php?g=api&m=user&a=withdraw", {
            userid: app.globalData.allData.userid,
            money: val,
            token: token
         }, (res) => {
            console.log(res)
            if (res.data.code == 2) {
               wx.showModal({
                  title: '提示',
                  content: '余额不足',
                  success: function (res) {
                     if (res.confirm) {
                        console.log('用户点击确定')
                     } else if (res.cancel) {
                        console.log('用户点击取消')
                     }
                  }
               })
            } else if (res.data.code == 3) {
               wx.showModal({
                  title: '提示',
                  content: '提现超额',
                  success: function (res) {
                     if (res.confirm) {
                        console.log('用户点击确定')
                     } else if (res.cancel) {
                        console.log('用户点击取消')
                     }
                  }
               })
            } else if (res.data.code == 1) {
               wx.showToast({
                  title: '提现成功',
                  icon: 'success',
                  duration: 2000
               })
               this.setData({
                  money_mask: false
               })
               wx.navigateBack({})
            } else {
               wx.showModal({
                  title: '提示',
                  content: '未知错误',
                  success: function (res) {
                     if (res.confirm) {
                        console.log('用户点击确定')
                     } else if (res.cancel) {
                        console.log('用户点击取消')
                     }
                  }
               })
            }
         }, (res) => { })
      }
   },
   // 输入提现金额
   money(e) {
      var val = e.detail.value;
      this.setData({
         val: val
      })
   },
   // 关闭莫泰框
   out() {
      this.setData({
         money_mask: false
      })
   },
   // 提现莫泰框空事件
   skip() { },
   // 提现
   withdraw_deposit() {
      this.setData({
         money_mask: true
      })
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {
      // 获取钱包接口
      app.ajax("/index.php?g=api&m=user&a=moneypay", {
         userid: app.globalData.allData.userid,
      }, (res) => {
         console.log(res)
         this.setData({
            allData: res.data
         })
      }, (res) => { })
   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {

   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})

