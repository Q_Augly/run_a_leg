// pages/ReplaceTake/ReplaceTake.js
var app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
   navActive:false,
  //  显示送件时间
    send_popup:false,
    // 显示物品大小
    product_size:false,
    // 显示订单池
    order_pool:false,
    // 授权弹框
    bjMoudel: true,
   //  订单池状态
    order_nav_wrap:false,
   //  订单池选项卡状态
    activeList:1,
   //  刷新订单
    orderList:[]
  },
//   接单按钮
   orderReceiving(e){
      var id = e.currentTarget.dataset.id;
      wx.showModal({
         title: '提示',
         content: '确认接受此订单?',
         success:  (res) => {
            if (res.confirm) {
               console.log('用户点击确定')
               app.ajax("/index.php?g=api&m=user&a=goodstype", {
                  states: 3,
                  goodsid: id,
                  userid: app.globalData.allData.userid,
               }, (res) => {
                  console.log(res)
                  if(res.data.code == 1){
                     this.onLoad()
                  }
               }, (res) => { })
            } else if (res.cancel) {
               console.log('用户点击取消')
            }
         }
      })
      
   },
//  选项卡
   activeList(e){
      wx.showLoading({
         title: '加载中',
      })

      var index = e.currentTarget.dataset.index;
      this.setData({
         activeList: index
      })
      var params = new Object;
      params.userid = app.globalData.allData.userid;
      params.areaid = app.globalData.areaid
      if (index == "2"){
         params.states = 1
      } else if (index == "3"){
         params.states = 2
      }
      app.ajax("/index.php?g=api&m=user&a=gogoods",params,(res)=>{
         console.log(res)
         if(res.data.goods == null){
            wx.showLoading({
               title: '空空如也',
               duration: 1000,
               mask:true
            })
         }else{
            this.setData({
               orderList: res.data.goods
            })
         }
         
      },(res)=>{})
   },
  show_popup:function(e){
    this.setData({
      send_popup:true
    })
  },
  hidden_popup(e){
    this.setData({
      send_popup: false
    })
  },

  show_size:function(e){
    this.setData({
      product_size: true
    })
  },
  hidden_size(e){
    this.setData({
      product_size: false
    })
  },
  show_orderpool:function(e){
     app.ajax("/index.php?g=api&m=user&a=gogoods",{
        userid: app.globalData.allData.userid,
        areaid: app.globalData.areaid
     },(res)=>{
        console.log(res)
        if(res.data.error == "1"){
           wx.navigateTo({
              url: '/pages/WaitAudit/WaitAudit',
           })
        } else if (res.data.error == "3"){
           wx.navigateTo({
              url: '/pages/NoAudit/NoAudit',
           })
        } else if (res.data.error == "4") {
           wx.showModal({
              title: '提示',
              content: '您已被关停，请联系管理员',
              success: function (res) {
                 if (res.confirm) {
                    console.log('用户点击确定')
                 } else if (res.cancel) {
                    console.log('用户点击取消')
                 }
              }
           })
        }else if (res.data.error == "0") {
           this.setData({
              order_pool: true
           })
        }else if(res.data.code == 1){
            this.setData({
               order_nav_wrap:true,
               order_pool: true
            })
        }
        
     },(res)=>{})
    
  },
  hidden_orderpool: function (e) {
    this.setData({
      order_pool: false
    })
  },
  run_register:function(e){
    wx.navigateTo({
      url: '/pages/RunRegister/RunRegister',
    })
  },
  SendDetails:function(e){
    wx.navigateTo({
      url: '/pages/SendDetails/SendDetails',
    })
  },
  SendDetails: function (e) {
    wx.navigateTo({
      url: '/pages/SendDetails/SendDetails',
    })
  },
  TakeDetails:function(e){
    wx.navigateTo({
      url: '/pages/TakeDetails/TakeDetails',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     this.setData({
        bjMoudel: app.globalData.bjMoudel
     })
     app.ajax("/index.php?g=api&m=user&a=gogoods", {
        userid: app.globalData.allData.userid,
        areaid: app.globalData.areaid
     }, (res) => {
        console.log(res)
        this.setData({
           orderList: res.data.goods
        })
     }, (res) => { })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})