// pages/RunRegister/RunRegister.js
var app = getApp()
Page({

   /**
    * 页面的初始数据
    */
   data: {
      phone: "",// 手机号
      phonecode: '', // 手机号验证码
      btntext: '获取验证码',
      btntextState: true,
      //  图片上传预览删除 
      allLength: 1,//  最多上传多少张
      getState: true,
      imgArray: [],
      // 判断注册需知
      agreement_img: false,
      // 学校
      areaidTitle: null,
      // 图片地址
      gopicurl: ""
   },
   // 表单提交
   bindMobile(e) {
      console.log(e.detail.value)
      var val = e.detail.value
      if (this.data.agreement_img == true) {
         if (val.name != "" && val.class != "" && val.go_mobile != "" && val.code != "" && this.data.gopicurl != "") {
            app.ajax("/index.php?g=api&m=user&a=gopeople", {
               name: val.name,
               userid: app.globalData.allData.userid,
               areaid: app.globalData.areaid,
               class: val.class,
               go_mobile: val.go_mobile,
               code: val.code,
               gopicurl: this.data.gopicurl
            }, (res) => {
               console.log(res)
               if(res.data == 1){
                  wx.showToast({
                     title: '成功',
                     icon: 'success',
                     duration: 2000
                  })
                  wx.navigateTo({
                     url: '/pages/WaitAudit/WaitAudit',
                  })
               } else if (res.data == 0){
                  wx.showModal({
                     title: '提示',
                     content: '请勿修改手机号码',
                     success: function (res) {
                        if (res.confirm) {
                           console.log('用户点击确定')
                        } else if (res.cancel) {
                           console.log('用户点击取消')
                        }
                     }
                  })
               } else if (res.data == 2) {
                  wx.showModal({
                     title: '提示',
                     content: '验证码错误',
                     success: function (res) {
                        if (res.confirm) {
                           console.log('用户点击确定')
                        } else if (res.cancel) {
                           console.log('用户点击取消')
                        }
                     }
                  })
               } else if (res.data == 3) {
                  wx.showModal({
                     title: '提示',
                     content: '手机号已注册',
                     success: function (res) {
                        if (res.confirm) {
                           console.log('用户点击确定')
                        } else if (res.cancel) {
                           console.log('用户点击取消')
                        }
                     }
                  })
               } else if (res.data == 4) {
                  wx.showModal({
                     title: '提示',
                     content: '未知错误,请稍后重试',
                     success: function (res) {
                        if (res.confirm) {
                           console.log('用户点击确定')
                        } else if (res.cancel) {
                           console.log('用户点击取消')
                        }
                     }
                  })
               } else if (res.data == -1) {
                  wx.showModal({
                     title: '提示',
                     content: '图片上传失败',
                     success: function (res) {
                        if (res.confirm) {
                           console.log('用户点击确定')
                        } else if (res.cancel) {
                           console.log('用户点击取消')
                        }
                     }
                  })
               }

               
            }, (res) => { })
         } else {
            wx.showLoading({
               title: '请填写信息',
               duration: 1000
            })
         }
      } else {
         wx.showModal({
            title: '提示',
            content: '请认真阅读注册须知！',
            success: function (res) {
               if (res.confirm) {
                  console.log('用户点击确定')
               } else if (res.cancel) {
                  console.log('用户点击取消')
               }
            }
         })
      }

   },
   // 判断注册需知
   agreement() {
      this.setData({
         agreement_img: !this.data.agreement_img
      })
   },
   //   选择图片
   getImg() {
      wx.chooseImage({
         count: this.data.allLength - this.data.imgArray.length, // 还可以上传几张
         sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
         sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
         success: (res) => {
            // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
            var tempFilePaths = res.tempFilePaths;
            var imgArray = this.data.imgArray
            for (let n in tempFilePaths) {
               // console.log(tempFilePaths[n])
               imgArray.push(tempFilePaths[n])
            }
            this.setData({
               imgArray: imgArray
            })
            console.log(imgArray)
            wx.uploadFile({
               url: 'http://dididida.net/index.php?g=api&m=user&a=picrul',
               filePath: this.data.imgArray[0],
               name: "file",
               formData: {
                  type: 1
               },
               success: (res) => {
                  // console.log(JSON.parse(res.data).picurl)
                  this.setData({
                     gopicurl: JSON.parse(res.data).picurl
                  })
               }
            })
            this.detection()
         }
      })
   },
   // 检测图片数量
   detection() {
      if (this.data.imgArray.length == this.data.allLength) {
         this.setData({
            getState: false
         })
      } else {
         this.setData({
            getState: true
         })
      }
   },
   // 删除
   del(e) {
      var index = e.currentTarget.dataset.index;
      var imgArray = this.data.imgArray;
      imgArray.splice(index, 1)
      this.setData({
         imgArray: imgArray
      })
      this.detection()
   },
   // 手机号码
   blur_mobile(event) { // 实时监听表单输入的值
      this.setData({
         phone: event.detail.value
      })
   },
   bindvalue(event) { // 实时监听表单输入的值
      this.setData({
         phonecode: event.detail.value
      })
   },
   // 点击获取验证码
   verification() { // 点击获取验证码
      app.ajax("/index.php?g=api&m=shop&a=code", {
         mobile: this.data.phone
      }, (res) => {
         console.log(res)
         if (res.data == 2) {
            wx.showModal({
               title: '提示',
               content: '发送失败,请正确填写手机号码',
               success: function (res) {
                  if (res.confirm) {
                     console.log('用户点击确定')
                  } else if (res.cancel) {
                     console.log('用户点击取消')
                  }
               }
            })
         } else {
            wx.showToast({
               title: '发送成功',
               icon: 'success',
               duration: 2000
            })
            var _this = this
            var coden = 60    // 定义60秒的倒计时
            var codeV = setInterval(function () {
               _this.setData({    // _this这里的作用域不同了
                  btntext: '重新获取' + (--coden) + 's',
                  btntextState: false
               })
               if (coden == -1) {  // 清除setInterval倒计时，这里可以做很多操作，按钮变回原样等
                  clearInterval(codeV)
                  _this.setData({
                     btntext: '获取验证码',
                     btntextState: true
                  })
               }
            }, 1000)
         }
      }, (res) => { })


      //  1000是1秒
   },

   // wait_audit: function (e) {
   //    wx.navigateTo({
   //       url: '/pages/WaitAudit/WaitAudit',
   //    })
   // },
   register_know: function (e) {
      wx.navigateTo({
         url: '/pages/RegisterKnow/RegisterKnow',
      })
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {
      console.log(app.globalData.areaidTitle)
      this.setData({
         areaidTitle: app.globalData.areaidTitle
      })
   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {

   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})