// pages/SearchEnd/SearchEnd.js
var app = getApp()
Page({

   /**
    * 页面的初始数据
    */
   data: {
      shopList: [],
      imgUrl: app.globalData.imgUrl,
      val: null
   },
   inputVal(e) {
      var val = e.detail.value;
      this.setData({
         val: val
      })
   },
   SearchEndPage: function (e) {
      if (this.data.val == null || this.data.val == ""){
         wx.showLoading({
            title: '填写搜索信息',
            duration: 2000
         })
      }else{
         wx.navigateTo({
            url: '/pages/SearchEnd/SearchEnd?val=' + this.data.val,
         })
      }
      
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {

      if (options.val) {
         console.log(options.val)
         app.ajax("/index.php?g=api&m=user&a=checkpage", {
            areaid: app.globalData.areaid,
            keyword: options.val
         }, (res) => {
            console.log(res)
            if (res.data.shop.length != 0) {
               this.setData({
                  shopList: res.data.shop
               })
            } else {
               wx.showLoading({
                  title: '空空如也',
                  duration: 2000
               })
               this.setData({
                  shopList: res.data.shop
               })
            }
         }, (res) => { })
      }
   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {

   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})