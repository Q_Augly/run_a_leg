// pages/SearchPage/SearchPage.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
     val:null
  },
  inputVal(e){
     var val = e.detail.value;
     this.setData({
        val: val
     })
  },
  SearchEndPage: function (e) {
     var val = this.data.val;
    if (val == null || val == "") {
       wx.showLoading({
          title: '填写搜索信息',
          duration: 2000
       })
    } else {
       wx.navigateTo({
          url: '/pages/SearchEnd/SearchEnd?val=' + this.data.val,
       })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

     app.ajax("/index.php?g=api&m=user&a=page",{
        userid: app.globalData.allData.userid,
        areaid:app.globalData.areaid
     },(res)=>{
        console.log(res)
        this.setData({
           allData:res.data
        })
     },(res)=>{})
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})