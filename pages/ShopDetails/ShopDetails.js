// pages/ShopDetails/ShopDetails.js
let app = getApp()
Page({

   /**
    * 页面的初始数据
    */
   data: {
      // 商品id
      shopid: null,
      // 所有信息
      shopDetails: [],
      headProduct: [
         {
            image: '/image/apic01@2x.png', sendTime: '起送 ￥0 | 配送 ￥2 | 30分钟', prompt: '公告：新店开业优惠多多，欢迎大家来品尝', reduce: '减', raduceDetails: '满20减10，满35减18'
         }
      ],
      raduceDetails: [],
      productNav: [
         { navName: '点餐必看', },
         { navName: '新款爆品' },
         { navName: '网红鲜果' },
         { navName: '千层盒子' },
      ],
      productItem: [
         {
            name: '夏日凉茶', introduce: '高颜值水果饮料', price: '￥21',
            typeList: [
               {
                  name: '夏日凉茶',
                  style: [
                     { size: '小杯' }
                  ]
               }
            ]
         },
         { name: '夏日凉茶', introduce: '高颜值水果饮料', price: '￥21' }
      ],
      cars: [],
      // 左侧导航选项卡index
      id: 0,
      tabArr: {
         tabNav: 0,
         tabItem: 0,
      },
      // 产品
      choose: false,
      // 购物车弹窗
      shopcar_popup: false,
      // 选规格
      choose_size: false,
      choose_id: null,
      // 规格杯号
      cup: 1,
      carNum: false,
      // 选择数量
      setNum: 1,
      imgUrl: app.globalData.imgUrl,
      //  被选择规格商品名
      listThisTitle: null,
      //  选择规格对应钱数
      typePrice: 0,
      shopCars:[]
   },
   // 获取商品规格参数接口
   getSizeData(count){
      app.ajax("/index.php?g=api&m=user&a=typelist",{
         userid: app.globalData.allData.userid,
         listid: this.data.choose_id,
         count: count
      },(res)=>{
         console.log(res)
      },(res)=>{})
   },
   // 清空购物车
   del(){
      // app.ajax("/index.php?g=api&m=user&a=goodscar", {
      //    listid: id,
      //    states: "add",
      //    typeid: this.data.cup,
      //    userid: app.globalData.allData.userid,
      // }, (res) => {
      //    console.log(res)
      //    var setNum = this.data.setNum + 1
      //    this.setData({
      //       setNum: setNum,
      //       typePrice: res.data.money
      //    })
      //    this.onLoad({ id: this.data.shopid })
      // }, (res) => { })
   },
   // 数量加
   add(e) {
      var id = e.currentTarget.dataset.id;
      if (this.data.setNum == 99) {
         var setNum = 99
         this.setData({
            setNum: setNum,
         })
      } else {
         app.ajax("/index.php?g=api&m=user&a=goodscar", {
            listid: id,
            states: "add",
            typeid: this.data.cup,
            userid: app.globalData.allData.userid,
         }, (res) => {
            console.log(res)
            var setNum = this.data.setNum + 1
            this.setData({
               setNum: setNum,
               typePrice: res.data.money
            })
            this.onLoad({ id: this.data.shopid })
         }, (res) => { })
      }

   },
   // 数量减
   dec(e) {
      var id = e.currentTarget.dataset.id;
      if (this.data.setNum == 1) {
         var setNum = 1
         this.setData({
            setNum: setNum,
         })
      } else {
         app.ajax("/index.php?g=api&m=user&a=goodscar", {
            listid: id,
            states: "dec",
            typeid: this.data.cup,
            userid: app.globalData.allData.userid,
         }, (res) => {
            console.log(res)
            var setNum = this.data.setNum - 1
            this.setData({
               setNum: setNum,
               typePrice: res.data.money
            })
            this.onLoad({ id: this.data.shopid })
         }, (res) => { })
      }

   },
   // 选择规格杯子
   clickCup(e) {
      var index = e.currentTarget.dataset.index;
      this.setData({
         cup: index
      })
      wx.showLoading({
         title: '加载中',
         mask: true
      })
      app.ajax("/index.php?g=api&m=user&a=goodscar", {
         listid: this.data.choose_id,
         states: "look",
         typeid: this.data.cup,
         userid: app.globalData.allData.userid,
      }, (res) => {
         wx.hideLoading()
         console.log(res)
         if (res.data.count == 0) {
            this.setData({
               carNum: false,
               setNum: 1
            })
         } else {
            this.setData({
               carNum: true,
               setNum: parseInt(res.data.list[0].num),
               typePrice: res.data.money
            })
         }
         this.onLoad({ id: this.data.shopid })
      }, (res) => { })
   },
   // 选择加入购物车个
   numCar(e) {
      var id = e.currentTarget.dataset.id;
      app.ajax("/index.php?g=api&m=user&a=goodscar", {
         listid: id,
         states: "save",
         typeid: this.data.cup,
         userid: app.globalData.allData.userid,
      }, (res) => {
         console.log(res)
         this.setData({
            carNum: true
         })
      }, (res) => { })
   },
   // tab选项卡
   tabFun: function (event) {
      var datasetId = event.target.dataset.id;
      var arr = {};
      arr.tabNav = datasetId;
      arr.tabItem = datasetId;
      console.log(arr)
      this.setData({
         tabArr: arr
      });
   },
   // 商品左侧导航点击效果
   navActive: function (event) {
      wx.showLoading({
         title: '加载中',
         mask:true
      })
      var index = event.currentTarget.dataset.index;
      var id = event.currentTarget.dataset.id;
      this.setData({
         id: index
      })
      app.ajax("/index.php?g=api&m=user&a=cutmenu",{
         userid: app.globalData.allData.userid,
         shop_menuid:id
      },(res)=>{
         wx.hideLoading()
         console.log(res)
         this.setData({
            productItem:res.data
         })
      },(res)=>{})
   },
   // 选择规格弹层
   showMask: function (e) {
      var id = e.currentTarget.dataset.id;
      var title = e.currentTarget.dataset.title;
      var count = e.currentTarget.dataset.count
      this.setData({
         choose: true,
         choose_size: !this.data.choose_size,
         choose_id: id,
         listThisTitle: title
      })
      wx.showLoading({
         title: '加载中',
         mask: true
      })
      app.ajax("/index.php?g=api&m=user&a=goodscar", {
         listid: this.data.choose_id,
         states: "look",
         typeid: this.data.cup,
         userid: app.globalData.allData.userid,
      }, (res) => {
         wx.hideLoading()
         console.log(res)
         if (res.data.count == 0) {
            this.setData({
               carNum: false,
               setNum: 1
            })
         } else {
            this.setData({
               carNum: true,
               setNum: parseInt(res.data.list[0].num),
               typePrice: res.data.money
            })
         }
      }, (res) => { })


      this.getSizeData(count)
   },
   hiddenMask: function (event) {
      this.setData({
         choose: false,
         choose_size: !this.data.choose_size,
      })
   },

   // 跳页
   newPage: function (e) {
      wx: wx.navigateTo({
         url: '/pages/order/order',
         success: function (res) { },
         fail: function (res) { },
         complete: function (res) { },
      })
   },
   // 购物车弹窗
   show_shopcar: function (e) {
      this.setData({
         shopcar_popup: !this.data.shopcar_popup,
         choose: !this.data.choose,
      })
      app.ajax("/index.php?g=api&m=user&a=goodscar", {
         shopid: this.data.shopid,
         states: "look",
         userid: app.globalData.allData.userid,
      }, (res) => {
         wx.hideLoading()
         console.log(res)
         this.setData({
            shopCars:res.data.list
         })
      }, (res) => { })
   },
   // 检测是否购物车有物品
   detection(){
      app.ajax("/index.php?g=api&m=user&a=goodscar", {
         shopid: this.data.shopid,
         states: "look",
         userid: app.globalData.allData.userid,
      }, (res) => {
         wx.hideLoading()
         console.log(res)
         this.setData({
            shopCars: res.data.list
         })
      }, (res) => { })
   },
   // 点击店铺中购物车列表数量增加
   shopCars(e){
      var state = e.currentTarget.dataset.state;
      var index = e.currentTarget.dataset.index;
      var type_id = e.currentTarget.dataset.type_id;
      var id = e.currentTarget.dataset.id;
      if (state == 1){
         console.log("减少")
         app.ajax("/index.php?g=api&m=user&a=goodscar", {
            carid: id,
            states: "dec",
         }, (res) => {
            console.log(res)
            app.ajax("/index.php?g=api&m=user&a=goodscar", {
               shopid: this.data.shopid,
               states: "look",
               userid: app.globalData.allData.userid,
            }, (res) => {
               wx.hideLoading()
               console.log(res)
               this.setData({
                  shopCars: res.data.list
               })
            }, (res) => { })
         }, (res) => { })

      } else if (state == 2){
         console.log("增加")
         app.ajax("/index.php?g=api&m=user&a=goodscar", {
            carid: id,
            states: "add",
         }, (res) => {
            console.log(res)
            app.ajax("/index.php?g=api&m=user&a=goodscar", {
               shopid: this.data.shopid,
               states: "look",
               userid: app.globalData.allData.userid,
            }, (res) => {
               wx.hideLoading()
               console.log(res)
               this.setData({
                  shopCars: res.data.list
               })
            }, (res) => { })
         }, (res) => { })

      }
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {
      //   店铺详情接口（列表和商品的小红点）
      if (options.id) {
         this.setData({
            shopid: options.id,
         })
         app.ajax("/index.php?g=api&m=user&a=details", {
            shopid: options.id,
            userid: app.globalData.allData.userid,
         }, (res) => {
            console.log(res)
            this.setData({
               headProduct: res.data.details,
               raduceDetails: res.data.sale,
               productNav: res.data.menu,
               productItem: res.data.list,
               cars: res.data.cars
            })
            wx.setNavigationBarTitle({
               title: res.data.details.name
            })
         }, (res) => { })
      }
   },
   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {
      this.detection()


     
   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})