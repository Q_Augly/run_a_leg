// pages/business/business.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
     phone:"",//手机号
     phonecode: '', // 手机号验证码
     btntext: '获取验证码',
     btntextState: true,
  },
  // 手机号码
  blur_mobile(event) { // 实时监听表单输入的值
     this.setData({
        phone: event.detail.value
     })
  },
  bindvalue(event) { // 实时监听表单输入的值
     this.setData({
        phonecode: event.detail.value
     })
  },
  verification() { // 点击获取验证码
     var phone = this.data.phone;
     if(phone != ""){
        app.ajax("/index.php?g=api&m=shop&a=code", {
           mobile: phone
        }, (res) => {
           console.log(res)
           if(res.data == 1){
              wx.showToast({
                 title: '发送成功',
                 icon: 'success',
                 mask:true,
                 duration: 2000
              })
              var _this = this
              var coden = 60    // 定义60秒的倒计时
              var codeV = setInterval(function () {
                 _this.setData({    // _this这里的作用域不同了
                    btntext: '重新获取' + (--coden) + 's',
                    btntextState: false
                 })
                 if (coden == -1) {  // 清除setInterval倒计时，这里可以做很多操作，按钮变回原样等
                    clearInterval(codeV)
                    _this.setData({
                       btntext: '获取验证码',
                       btntextState: true
                    })
                 }
              }, 1000)  //  1000是1秒
           } else if (res.data == 2){
              wx.showLoading({
                 title: '发送失败',
                 mask: true,
                 duration: 1000
              })

           }
        }, (res) => { })
     }else{
        wx.showLoading({
           title: '请输入手机号码',
           duration: 1000
        })
     }
     
     
  }, 
  BusinessCenter:function(e){
     if (this.data.phonecode != ""){
        app.ajax("/index.php?g=api&m=shop&a=login", {
           mobile:this.data.phone,
           phonecode: this.data.phonecode
        }, (res) => { 
           console.log(res)
           if (res.data.code == "missmobile"){
              wx.showModal({
                 title: '提示',
                 content: '手机号码不存在',
                 success: function (res) {
                    if (res.confirm) {
                       console.log('用户点击确定')
                    } else if (res.cancel) {
                       console.log('用户点击取消')
                    }
                 }
              })
           } else if (res.data.code == "misscode"){
              wx.showModal({
                 title: '提示',
                 content: '验证码错误',
                 success: function (res) {
                    if (res.confirm) {
                       console.log('用户点击确定')
                    } else if (res.cancel) {
                       console.log('用户点击取消')
                    }
                 }
              })
           } else if (res.data.code == "closeshop") {
              wx.showModal({
                 title: '提示',
                 content: '店铺被关停联系管理员',
                 success: function (res) {
                    if (res.confirm) {
                       console.log('用户点击确定')
                    } else if (res.cancel) {
                       console.log('用户点击取消')
                    }
                 }
              })
           } else if (res.data.code == "unableshop") {
              wx.showModal({
                 title: '提示',
                 content: '未审核通过',
                 success: function (res) {
                    if (res.confirm) {
                       console.log('用户点击确定')
                    } else if (res.cancel) {
                       console.log('用户点击取消')
                    }
                 }
              })
           } else if (res.data.code == "shopid") {
              console.log("验证成功返回商户id")
              console.log(res)
            //   wx.navigateTo({
            //      url: '/pages/BusinessCenter/BusinessCenter',
            //   })
           }
        }, (res) => { })
     }
     
       
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})