let app = getApp();
const config = require('../../config.js');
Page({

   /**
    * 页面的初始数据
    */
   data: {
      // 地址选择弹框
      choose: false,
      selectIndex: 0,
      array: [],
      array_sub: [],
      // tab
      TabItem: 1,
      areaid: null,
      imgUrl: app.globalData.imgUrl,
      // 授权弹框
      bjMoudel: app.globalData.bjMoudel,
   },
// 分类跳转
   indexType(e){
      var index = e.currentTarget.dataset.index;
      wx.reLaunch({
         url: '../Classification/Classification?index='+index,
      })
   },
   // 授权获取信息
   onGotUserInfo(e) {
      var userInfo = e.detail.userInfo;
      app.globalData.userInfo = userInfo;

      wx.login({
         success: (res) => {
            app.ajax(app.globalData.indexPage, {
               code: res.code,
               avatarUrl: app.globalData.userInfo.avatarUrl,
               nickName: app.globalData.userInfo.nickName,
            }, (res) => {
               console.log(res)
               this.setData({
                  bjMoudel: false
               })
               app.globalData.bjMoudel = false
            }, (res) => { })
         },
         fail: (res) => { },
         complete: (res) => { },
      })
      console.log(userInfo)
   },

   show: function (event) {
      this.setData({
         choose: !this.data.choose
      })
   },
   school(event) {
      this.setData({
         selectIndex: event.target.dataset.index,
         choose: !this.data.choose,
         areaid: event.target.dataset.areaid,
      })
      console.log(event.target.dataset.areaid)
      console.log(event.target.dataset.title)
      app.globalData.areaid = event.target.dataset.areaid
      app.globalData.areaidTitle = event.target.dataset.title
      wx.showLoading({
         title: '加载中',
         mask: true
      })
      var params = new Object;
      params.areaid = this.data.areaid;
      params.rankid = this.data.TabItem;
      if (params.areaid == 3) {
         params.length = app.globalData.coordinate;
      }
      app.ajax("/index.php?g=api&m=user&a=rank", params, (res) => {
         console.log(res)
         this.setData({
            "array.shop": res.data
         })
         wx.hideLoading()
      }, (res) => { })
   },

   // 选项卡
   tabFun: function (event) {
      var that = this
      var _datasetId = event.target.dataset.rankid;
      this.setData({
         TabItem: _datasetId
      });
      wx.showLoading({
         title: '加载中',
         mask: true
      })
      var params = new Object;
      params.areaid = this.data.areaid;
      params.rankid = this.data.TabItem;
      if (params.areaid == 3) {
         params.length = app.globalData.coordinate;
      }
      app.ajax("/index.php?g=api&m=user&a=rank", params, (res) => {
         console.log(res)
         that.setData({
            "array.shop": res.data
         })
         wx.hideLoading()
      }, (res) => { })

   },


   // lookres: function (event) {
   //    console.log(event.currentTarget.dataset.id)
   // },
   shop_details: function (e) {
      var id = e.currentTarget.dataset.id;
      wx.navigateTo({
         url: '/pages/ShopDetails/ShopDetails?id=' + id,
         success: function (res) { },
         fail: function (res) { },
         complete: function (res) { },
      })
   },


   /**
    * 生命周期函数--监听页面加载
    */
   onLoad(options) {
      var that = this
      //获取code
      wx.login({
         success: (res) => {
            app.ajax(app.globalData.indexPage, {
               code: res.code
            }, (res) => {
               console.log(res)
               app.globalData.allData = res.data
               that.setData({
                  array: app.globalData.allData,
                  areaid: app.globalData.allData.area[0].id
               })
               app.globalData.areaid = app.globalData.allData.area[0].id
               app.globalData.areaidTitle = app.globalData.allData.area[0].name
               if (res.data.code == "-2") {
                  console.log("需要授权")
               } else {
                  that.setData({
                     bjMoudel: false
                  })
                  app.globalData.bjMoudel = false
               }
            }, (res) => { })
         },
         fail: (res) => { },
         complete: (res) => { },
      })

      // 获取经纬度
      wx.getLocation({
         type: 'wgs84',
         success: function (res) {
            var latitude = res.latitude
            var longitude = res.longitude
            var speed = res.speed
            var accuracy = res.accuracy
            app.globalData.coordinate = { "lng": longitude, "lat": latitude }
         }
      })



   },


   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {

   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})